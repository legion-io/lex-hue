require 'legion/extensions/hue/version'

module Legion
  module Extensions
    module Hue
      extend Legion::Extensions::Core if Legion::Extensions.const_defined? :Core
    end
  end
end
